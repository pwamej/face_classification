FROM tensorflow/tensorflow:latest-py3

RUN apt-get -y update && apt-get -y upgrade
RUN apt-get install -y python3 python3-dev python3-pip libopencv-dev python3-tk
RUN pip3 install --no-cache --upgrade pip
RUN pip3 install --no-cache pipenv

WORKDIR /face-classifier

ADD requirements.txt ./
RUN pip3 install -r requirements.txt

COPY . .

WORKDIR /face-classifier/src
ENTRYPOINT ["python3"]
CMD ["video_emotion_color_demo.py"]
