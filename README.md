# R2D2 emotion detection

*Forked from https://github.com/oarriaga/face_classification*


## Setup
```bash
pipenv install
pipenv shell
```

## Run
```bash
cd src
python emotion_detection.py [MAX_FACES]
```
