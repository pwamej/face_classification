import json
import os
from collections import deque

import pendulum
from requests_futures.sessions import FuturesSession

from emotion_detection import SUBMIT_INTERVAL_IN_SECONDS

API_TOKEN = os.environ["R2D2_API_TOKEN"]
HOST = 'https://krksite.eu.qualtrics.com'
SURVEY_ID = 'SV_72oY6f1DEGQ15Gd'
DATE_FORMAT = 'YYYY-MM-DDTHH:mm:ss\Z'

START_YEAR = 2000
START_DATE = pendulum.datetime(START_YEAR, 1, 1)

session = FuturesSession()

DASHBOARD_SIZE_IN_SECONDS = 60
response_id_queue = deque()


def print_post_response(resp, *args, **kwargs):
    print(resp.text)
    response_id_queue.append(resp.json()['result']['responseId'])


def print_delete_response(resp, *args, **kwargs):
    print(resp.text)


def submit_emotions(emotions, valid_emotions):
    global START_DATE
    START_DATE = START_DATE.add(days=1)
    print(emotions)

    if valid_emotions:
        upload_response(emotions)
    else:
        response_id_queue.append('')

    while len(response_id_queue) > (DASHBOARD_SIZE_IN_SECONDS / SUBMIT_INTERVAL_IN_SECONDS):
        delete_response(response_id_queue.popleft())


def upload_response(emotions):
    end_date = START_DATE
    payload = json.dumps({"values": {
        "startDate": end_date.subtract(minutes=1).format(DATE_FORMAT),
        "endDate": end_date.format(DATE_FORMAT),
        "status": 4,
        "progress": 100,
        "duration": 7,
        "finished": 1,
        "recordedDate": end_date.format(DATE_FORMAT),
        "locationLatitude": "40.2319030762",
        "locationLongitude": "-111.675498962",
        "distributionChannel": "preview",
        "userLanguage": "EN",
        "QID2_1": emotions["angry"],
        "QID2_7": emotions["disgust"],
        "QID2_10": emotions["fear"],
        "QID2_11": emotions["happy"],
        "QID2_12": emotions["sad"],
        "QID2_13": emotions["surprise"],
        "QID2_14": emotions["neutral"]
    }})

    headers = {
        'accept': "application/json",
        'content-type': "application/json",
        'X-API-TOKEN': API_TOKEN
    }

    session.post(f'{HOST}/API/v3/surveys/{SURVEY_ID}/responses', data=payload, headers=headers, hooks={
        'response': print_post_response
    })

    return


def delete_response(response_id):
    headers = {
        'accept': "application/json",
        'content-type': "application/json",
        'X-API-TOKEN': API_TOKEN
    }

    session.delete(f'{HOST}/API/v3/surveys/{SURVEY_ID}/responses/{response_id}', headers=headers, hooks={
        'response': print_delete_response
    })
