import sys
from collections import Counter, deque
from statistics import mode

import click
import cv2
import pendulum
from keras.models import load_model
import numpy as np

import qualtrics_client
from utils.datasets import get_labels
from utils.inference import detect_faces
from utils.inference import draw_text
from utils.inference import draw_bounding_box
from utils.inference import apply_offsets
from utils.inference import load_detection_model
from utils.preprocessor import preprocess_input
from imutils.video import WebcamVideoStream

# parameters for loading data and images
detection_model_path = '../trained_models/detection_models/haarcascade_frontalface_default.xml'
emotion_model_path = '../trained_models/emotion_models/fer2013_mini_XCEPTION.102-0.66.hdf5'
emotion_labels = get_labels('fer2013')

# hyper-parameters for bounding boxes shape
emotion_offsets = (20, 40)

FPS = 24
SUBMIT_INTERVAL_IN_SECONDS = 3

# display emotion that was detected most often in past N successful detections
detected_emotions = deque(maxlen=5)


@click.command()
@click.option('--max-faces', default=1, help="Maximum number of faces that can be detected at the same time")
@click.option('-h', '--pi-host', help="Address of Raspberry Pi host", required=True)
def main(max_faces, pi_host):
    # loading models
    face_detection = load_detection_model(detection_model_path)
    emotion_classifier = load_model(emotion_model_path, compile=False)

    # getting input model shapes for inference
    emotion_target_size = emotion_classifier.input_shape[1:3]

    # starting video streaming
    cv2.namedWindow('QED', flags=cv2.WINDOW_NORMAL | cv2.WINDOW_GUI_EXPANDED)
    video_capture = WebcamVideoStream(f"http://{pi_host}:8000/stream.mjpg").start()

    previous_emotion_submit = pendulum.now()
    successful_predictions = 0
    mean_predictions = np.zeros(len(emotion_labels))
    while True:
        bgr_image = cv2.flip(video_capture.read(), 1)
        gray_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2GRAY)
        rgb_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2RGB)

        faces = detect_faces(face_detection, gray_image)
        emotion_prediction = np.zeros(len(emotion_labels))
        if len(faces) > 0:
            successful_predictions += 1
            for face_coordinates in faces[:max_faces]:
                x1, x2, y1, y2 = apply_offsets(face_coordinates, emotion_offsets, rgb_image.shape)
                gray_face = gray_image[y1:y2, x1:x2]
                gray_face = cv2.resize(gray_face, emotion_target_size)

                gray_face = preprocess_input(gray_face, True)
                gray_face = np.expand_dims(gray_face, 0)
                gray_face = np.expand_dims(gray_face, -1)
                emotion_prediction = emotion_classifier.predict(gray_face)
                draw_emotion(emotion_prediction, rgb_image, face_coordinates)

                bgr_image = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2BGR)
                emotion_prediction = emotion_prediction[0]
        mean_predictions += emotion_prediction
        if (pendulum.now() - previous_emotion_submit).seconds > SUBMIT_INTERVAL_IN_SECONDS:
            previous_emotion_submit = pendulum.now()

            mean_predictions /= successful_predictions
            successful_predictions = 0
            average_detected_emotions = dict(zip(emotion_labels.values(), mean_predictions))
            print(", ".join(f"{label}: {100 * val:.2f}" for label, val in average_detected_emotions.items()))

            valid_emotions = not np.isnan(mean_predictions).any()

            qualtrics_client.submit_emotions(average_detected_emotions, valid_emotions)

            mean_predictions = np.zeros(len(emotion_prediction))

        cv2.imshow('QED', bgr_image)
        cv2.waitKey(int(1000 / FPS))


def draw_emotion(emotion_prediction, rgb_image, face_coordinates):
    emotion_probability = np.max(emotion_prediction)
    emotion_text = emotion_labels[np.argmax(emotion_prediction)]
    detected_emotions.append(emotion_text)
    emotion_mode = Counter(detected_emotions).most_common(1)[0][0]
    color = color_for_emotion(emotion_mode, emotion_probability)
    draw_bounding_box(face_coordinates, rgb_image, color)
    draw_text(face_coordinates, rgb_image, emotion_mode, color, 0, -45, 1, 1)


def color_for_emotion(emotion_text, emotion_probability):
    if emotion_text == 'angry':
        color = emotion_probability * np.asarray((255, 0, 0))
    elif emotion_text == 'sad':
        color = emotion_probability * np.asarray((0, 0, 255))
    elif emotion_text == 'happy':
        color = emotion_probability * np.asarray((255, 255, 0))
    elif emotion_text == 'surprise':
        color = emotion_probability * np.asarray((0, 255, 255))
    else:
        color = emotion_probability * np.asarray((0, 255, 0))
    return color.astype(int).tolist()


if __name__ == "__main__":
    main()
